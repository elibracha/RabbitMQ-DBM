# RabbitMQ-DBM

This is an example repository written in python, with demonstrate the use of RabbitMQ together with SQLite to query and manipulate
the chinook database example provided by the sqlite team.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- local RabbitMQ server installed
- local sqlite database install
- download of the chinook.db file provided from sqlite team
- local python interpreter 2.7


### Suppoted formats

just like it was required the supported formats for this project is the following 4 (format can be changed in the client module) :

- csv (look for result in the created 'output' folder)
- xml (look for result in the created 'output' folder)
- json (look for result in the created 'output' folder)
- table (create table in DB from the result queries)

### How to run?

The project have 2 modules. to run the project, just run the server module file and client module file <br>
(the running order doesn't matter).<br>
after that see the output files in the output folder.
```

## Authors

* ** Eli Bracha ** - *Full Work* - [elibracha](https://github.com/elibracha)
