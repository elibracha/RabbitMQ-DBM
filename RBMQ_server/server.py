'''
Created on Oct 4, 2018

@author: elibracha
'''

import pika

from RBMQ_server.DB_Manager import DB_Manger
from RBMQ_server.IO_Manager import IO_Manger


def connect_queue():
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    
    channel.queue_declare(queue='chinook-db')
    
    return channel

def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)
    params = body.split(',')
    db_manager = DB_Manger(params[0])
 
    print("Qurey Request ...")
    
    if params[1] == 'xml':
        rows, cursor = db_manager.execute_query(db_manager.GET_TRACKS_BY_COUNTRY)
        IO_Manger.create_xml(rows, cursor, 'GET_TRACKS_BY_COUNTRY')
        rows, cursor = db_manager.execute_query(db_manager.GET_AMARICAN_INVOICES)
        IO_Manger.create_xml(rows, cursor, 'GET_AMARICAN_INVOICES')
        rows, cursor = db_manager.execute_query(db_manager.GET_MAX_INVOICE_BY_COUNTRY)
        IO_Manger.create_xml(rows, cursor, 'GET_MAX_INVOICE_BY_COUNTRY')
        rows, cursor = db_manager.execute_query(db_manager.GET_BEST_TRACK_SELLER)
        IO_Manger.create_xml(rows, cursor, 'GET_BEST_TRACK_SELLER')
        rows, cursor = db_manager.execute_query(db_manager.GET_MAX_INVOICE_STATE)
        IO_Manger.create_xml(rows, cursor, 'GET_MAX_INVOICE_STATE')
    elif params[1] == 'csv':
        rows, cursor = db_manager.execute_query(db_manager.GET_TRACKS_BY_COUNTRY)
        IO_Manger.create_csv(rows, cursor, 'GET_TRACKS_BY_COUNTRY')
        rows, cursor = db_manager.execute_query(db_manager.GET_AMARICAN_INVOICES)
        IO_Manger.create_csv(rows, cursor, 'GET_AMARICAN_INVOICES')
        rows, cursor = db_manager.execute_query(db_manager.GET_MAX_INVOICE_BY_COUNTRY)
        IO_Manger.create_csv(rows, cursor, 'GET_MAX_INVOICE_BY_COUNTRY')
        rows, cursor = db_manager.execute_query(db_manager.GET_BEST_TRACK_SELLER)
        IO_Manger.create_csv(rows, cursor, 'GET_BEST_TRACK_SELLER')
        rows, cursor = db_manager.execute_query(db_manager.GET_MAX_INVOICE_STATE)
        IO_Manger.create_csv(rows, cursor, 'GET_MAX_INVOICE_STATE')
    elif params[1] == 'json':
        rows, cursor = db_manager.execute_query(db_manager.GET_TRACKS_BY_COUNTRY)
        IO_Manger.create_json(rows, cursor, 'GET_TRACKS_BY_COUNTRY')
        rows, cursor = db_manager.execute_query(db_manager.GET_AMARICAN_INVOICES)
        IO_Manger.create_json(rows, cursor, 'GET_AMARICAN_INVOICES')
        rows, cursor = db_manager.execute_query(db_manager.GET_MAX_INVOICE_BY_COUNTRY)
        IO_Manger.create_json(rows, cursor, 'GET_MAX_INVOICE_BY_COUNTRY')
        rows, cursor = db_manager.execute_query(db_manager.GET_BEST_TRACK_SELLER)
        IO_Manger.create_json(rows, cursor, 'GET_BEST_TRACK_SELLER')
        rows, cursor = db_manager.execute_query(db_manager.GET_MAX_INVOICE_STATE)
        IO_Manger.create_json(rows, cursor, 'GET_MAX_INVOICE_STATE')
    elif params[1] == 'table':
        IO_Manger.create_table(db_manager.GET_TRACKS_BY_COUNTRY, db_manager, 'GET_TRACKS_BY_COUNTRY')
        IO_Manger.create_table(db_manager.GET_AMARICAN_INVOICES, db_manager, 'GET_AMARICAN_INVOICES')
        IO_Manger.create_table(db_manager.GET_BEST_TRACK_SELLER, db_manager, 'GET_BEST_TRACK_SELLER')
        IO_Manger.create_table(db_manager.GET_MAX_INVOICE_STATE, db_manager, 'GET_MAX_INVOICE_STATE')
        IO_Manger.create_table(db_manager.GET_MAX_INVOICE_BY_COUNTRY, db_manager, 'GET_MAX_INVOICE_BY_COUNTRY')

    print("Done Processing Request.")
      
    
def start_server():
    channel = connect_queue()
    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.basic_consume(callback,
                      queue='chinook-db',
                      no_ack=True)
    channel.start_consuming()
   
 
   
if __name__ == "__main__":
    start_server()