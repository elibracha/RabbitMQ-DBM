'''
Created on Oct 4, 2018

@author: elibracha
'''

from sqlite3 import Error
import sqlite3


class DB_Manger(object):
    
    '''
    Queries requested for the task -
    - GET_TRACKS_BY_COUNTRY - select how much tracks was sold per country.
    - GET_AMARICAN_INVOICES - gets a list of all american customers who bought tracks.
    - GET_MAX_INVOICE_BY_COUNTRY - get the country with the most customer sells.
    - GET_BEST_TRACK_SELLER - get the best selling disk (or couple of disks if sells are the same).
    - GET_MAX_INVOICE_STATE - gets the state with the most sells in USA.
    '''
    GET_TRACKS_BY_COUNTRY      = "SELECT Country, COUNT(invoice_items.invoiceId) AS total_tracks FROM customers NATURAL JOIN invoices NATURAL JOIN invoice_items GROUP BY Country;"
    GET_AMARICAN_INVOICES      = "SELECT CustomerId, FirstName, LastName, Address, Phone, Fax, Email, PostalCode, State, City FROM customers NATURAL JOIN invoices WHERE Country = 'USA' GROUP BY CustomerId;"
    GET_MAX_INVOICE_BY_COUNTRY = "SELECT Country, MAX(total) AS total_sells FROM (SELECT Country, COUNT(invoice_items.invoiceId) AS total FROM customers NATURAL JOIN invoices NATURAL JOIN invoice_items GROUP BY Country)"
    GET_BEST_TRACK_SELLER      = "SELECT TrackId, Name, COUNT(TrackId) AS sells FROM invoice_items NATURAL JOIN tracks GROUP BY TrackId HAVING COUNT(TrackId) >= (SELECT MAX(tmp.total) FROM (SELECT COUNT(TrackId) AS total FROM invoice_items NATURAL JOIN tracks GROUP BY TrackId) AS tmp);"
    GET_MAX_INVOICE_STATE      = "SELECT State, MAX(total) AS total_sells FROM (SELECT State, COUNT(invoice_items.invoiceId) AS total FROM customers NATURAL JOIN invoices NATURAL JOIN invoice_items WHERE Country = 'USA' GROUP BY State);"
   
    '''
    Queries needed to create new tables if format 'table' was entered
    '''
    GET_ALL_TABLES             = "SELECT name FROM sqlite_master WHERE type='table';"
    DROP_TABLE                 = "DROP TABLE %s;"
    CREATE_TABLE               = "CREATE TABLE %s AS %s"
    
    
    def __init__(self, db_file = "chinook.db"):
        self.connection = self.__create_connection__(db_file)
        
    
    def __create_connection__(self, db_file):
        """ create a database connection to the SQLite database
            specified by the db_file
        :param db_file: database file
        :return: Connection object or None
        """
        try:
            conn = sqlite3.connect(db_file)
            return conn
        except Error as e:
            print(e)
     
        return None
     
     
    def execute_query(self, query):
        """
        Query all rows in the tasks table
        :return: the rows received from the QUery 
        """
        cursor = self.connection.cursor()
        cursor.execute(query)
    
        return cursor.fetchall(), cursor
    