'''
Created on Oct 4, 2018

@author: elibracha
'''

import csv, json
import os

import xml.etree.cElementTree as ET


class IO_Manger(object):
    
    '''
    This variable is the default folder of the generated output.
    '''
    _directory_ = "output"

    @classmethod
    def __directory_setup__(cls):
        if not os.path.exists(cls._directory_):
            os.makedirs(cls._directory_)
            
    @classmethod
    def create_xml(cls, rows, cursor, filename):
        """
        creating a xml file form the Query.
        :param rows: the Rows returned from the query
        :param cursor: the cursor created from the query
        :param filename: the filename that will be created
        :return:
        """
        cls.__directory_setup__()
        
        root = ET.Element(filename)
        
        for idx, row in enumerate(rows):
            doc = ET.SubElement(root, 'row-%s' % idx)
            index = 0
            for i in cursor.description:
                ET.SubElement(doc, i[0]).text = unicode(row[index])
                index += 1
                
            tree = ET.ElementTree(root)
            
            if not os.path.exists(cls._directory_):
                os.makedirs(cls._directory_)
                
            tree.write("output/%s.xml" % filename)
        
    @classmethod    
    def create_csv(cls, rows, cursor, filename):
        """
        creating a csv file form the Query.
        :param rows: the Rows returned from the query
        :param cursor: the cursor created from the query
        :param filename: the filename that will be created
        :return:
        """
        
        cls.__directory_setup__()
        
        with open("output/%s.csv" % filename, "w") as csv_file:
            csv_writer = csv.writer(csv_file)
            csv_writer.writerow([i[0] for i in cursor.description]) # write headers
            for row in rows:
                csv_writer.writerow([unicode(s).encode('utf8') for s in row])    
         
     
    @classmethod
    def create_json(cls, rows, cursor, filename):
        """
        creating a Json file form the Query.
        :param rows: the Rows returned from the query
        :param cursor: the cursor created from the query
        :param filename: the filename that will be created
        :return:
        """
        
        cls.__directory_setup__()
        
        data = {}
        object_list = []
        for idx, row in enumerate(rows):
            emp = {}
            row_data = {}
            
            index = 0
            for i in cursor.description:
                row_data[i[0]] = unicode(row[index])
                index += 1
                
            emp['row-%s' % idx] = row_data
            object_list.append(emp)
            
        data[filename] = object_list
        
        with open("output/%s.json" % filename, 'w') as outfile:
            json.dump(data, outfile, indent=4)
     
     
    @classmethod       
    def create_table(cls, query, db_manger, table_name):
        """
        creating a Table in the database form the Query.
        :param table_name: the table_name that will be created
        :return:
        """
        
        rows, _ = db_manger.execute_query(db_manger.GET_ALL_TABLES)
        
        for row in rows:
            if row[0] is table_name or row[0] == table_name:
                db_manger.execute_query(db_manger.DROP_TABLE % table_name)
        
        db_manger.execute_query(db_manger.CREATE_TABLE % (table_name, query))
        
        
        
            
            