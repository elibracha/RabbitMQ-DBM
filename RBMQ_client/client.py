'''
Created on Oct 4, 2018

@author: elibracha
'''
import pika 


def queue_request(db_host, formt):
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    
    channel.queue_declare(queue='chinook-db')
    message = db_host + ',' + formt
    channel.basic_publish(exchange='',
                        routing_key='chinook-db',
                     body=message)
    
    print(" Producer Sent - " + message)
        
    connection.close()
    
if __name__ == '__main__':

    db_host = "/home/elibracha/Databases/chinook.db"
    formt = "table"
    
    queue_request(db_host, formt)
    
    